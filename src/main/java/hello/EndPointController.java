package hello;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Calendar;
import java.util.ArrayList;
import java.util.HashMap;

@RestController
public class EndPointController {

    private static final String template = "Hello, %s!";
    @Autowired 	private ClienteCRUD clienteCRUD;
    @Autowired 	private UtenteCRUD utenteCRUD;

    
    @RequestMapping(value="/savejson",method = RequestMethod.POST,headers="Content-Type=application/json")
    public HashMap<String, Boolean>   save(@RequestBody ArrayList<Cliente> utenti) {

        HashMap<String, Boolean> rtn = new HashMap<String, Boolean>();
      
        try
        {
            for (Cliente ut : utenti) {
                    clienteCRUD.save(ut);
                    rtn.put("status", true);   
            }
        }catch(Exception e) { 
            rtn.put("status", false);
        }

    return rtn;

    }

    @RequestMapping("/listclienti")
    public ArrayList<Cliente> get() {

        ArrayList<Cliente> lista = new ArrayList<Cliente>();
        clienteCRUD.findAll().forEach(e -> lista.add(e));  
        return lista;
    }

    @RequestMapping(value="/login",method = RequestMethod.POST,headers="Content-Type=application/json")
    public Utente  login(@RequestBody Utente utente) {

        //SIMULO OAUTH2
        //Recupero manualmente dal DB l'utente che risponde alle credenziali
        //e ritorno un access-token-fake
        return this.checkAuth(utente);
    }



    //ATTENZIONE SIMULO OAUTH2 -> questo è un metodo d'appoggio
    public Utente checkAuth(Utente u)
    {
        Iterable<Utente> utenti = utenteCRUD.findAll();  
        
        for (Utente ut : utenti) {
            if(ut.getUsername().equals(u.getUsername()) && ut.getPassword().equals(u.getPassword())) {
                ut.setPassword("**OFF**");
                return ut;
            }
        }
        return u;
    }

    
}