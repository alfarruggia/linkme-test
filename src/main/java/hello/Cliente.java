package hello;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Calendar;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor

public class Cliente {
    
    @Id @GeneratedValue (strategy = GenerationType.SEQUENCE) Long id;
    String nome="";
    String cognome="";
    Calendar nascita=Calendar.getInstance();
    String citta="";
    String cf="";

}