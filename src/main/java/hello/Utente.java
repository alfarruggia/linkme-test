package hello;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Utente {
    
    @Id @GeneratedValue (strategy = GenerationType.SEQUENCE) Long id;

    @JsonProperty("username")
    @Getter @Setter private String username="";

    @JsonProperty("password")
    @Getter @Setter private String password="";

    @JsonProperty("access_token")
    @Getter @Setter private String access_token="";
}