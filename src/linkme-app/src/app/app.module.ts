import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

//Imporatti per il progetto
import { HttpClientModule } from '@angular/common/http';
import { FormsModule }   from '@angular/forms';
import { LinkmeService } from "./linkme.service";
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { UploadComponent } from './upload/upload.component';
import { ListComponent } from './list/list.component';
import { LoginComponent } from './login/login.component';
import { DummyMenuComponent } from './dummy-menu/dummy-menu.component';

//routing conf
const appRoutes: Routes = [
  { path: 'app-login',   component: LoginComponent },
  { path: 'app-home',   component: HomeComponent },
  { path: 'app-list',   component: ListComponent },
  { path: 'app-upload', component: UploadComponent },  
  { path: '**',         component: LoginComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UploadComponent,
    ListComponent,
    LoginComponent,
    DummyMenuComponent
  ],
  imports: [
    RouterModule.forRoot( appRoutes, { enableTracing: true } ),
    BrowserModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [LinkmeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
