import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-dummy-menu',
  templateUrl: './dummy-menu.component.html',
  styleUrls: ['./dummy-menu.component.css']
})
export class DummyMenuComponent implements OnInit {
  @Input()
  title:String;

  constructor() { }

  ngOnInit() {
  }

}
