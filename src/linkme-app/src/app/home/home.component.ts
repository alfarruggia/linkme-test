import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DummyMenuComponent } from '../dummy-menu/dummy-menu.component';
import { LinkmeService } from "../linkme.service";
import 'rxjs/Rx' ;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router,private lms: LinkmeService) { }

  ngOnInit() {
  }

  createBloB()
  {
    var self=this;
    this.lms.download().subscribe(data => {
      var blob = new Blob([JSON.stringify(data)], { type: 'text/json' });
      var url= window.URL.createObjectURL(blob);
      window.open(url);
      });
  }

}
