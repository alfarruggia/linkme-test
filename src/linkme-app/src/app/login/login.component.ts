import { Component, OnInit } from '@angular/core';
import { LinkmeService } from "../linkme.service";
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private lms: LinkmeService,private router: Router) {}

  ngOnInit() {
  }

  doLogin(lf:NgForm)
  {
    var self=this;
    this.lms.login(lf.value).subscribe(data => {
      
      if (data.hasOwnProperty('access_token')) {
        self.lms.setUser(data)
        self.router.navigate(['app-home'])     
      }

    });
  }
}
