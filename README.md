# LINK-ME TEST

## Configurazione
### Creazione DB msql
* $> mysqld
* create database testlinkme;
* create user 'test'@'localhost' identified by '123456'; 
* grant all on testlinkme.* to 'test'@'localhost'; 

### Autenticazione
* Sistema di autenticazione **Dummy** - simulazione OAUTH2
    * Il server riceve le credenziali durante il login, controlla nomeutente e password e restituisce un oggetto contenente l'access-token.
    * ... poi il client non lo usa :)
* Creazione  tabella con le credenziali per l'utente
    * $> mysql
    * use testlinkme;
    * CREATE TABLE utente ( id INT NOT NULL AUTO_INCREMENT, username VARCHAR(100) NOT NULL, password VARCHAR(40) NOT NULL, access_token VARCHAR(40) NOT NULL , PRIMARY KEY ( id ));
    * INSERT INTO utente (username,password,access_token) values ('user1','MD5pass1','my-acc3ss-t0k3n-user1');

## Framewwork
### Server
* **Spring**
    * Librerie
    	* Lombok
    	* Jackson
    * Path sorgente src/java/hello
    * Modelli: Cliente.java, Utente.java
    * EndPointController.java : gestisce le chiamate Rest e simula autenticazione
    * Classi estese da CrudRepository per Utente e Cliente
    * src/main/resources/application.properties : parametri configurazione Mysql
    
### Client
* **Angular5**
    * Path sorgente src/linkme-app
    * Path JS compilato src/main/resources/static
    * Uso minimale di Boostrap
    * Componenti
        * Login 
        * Home (routing verso **Upload** e **List**, la funzione **Download** -> Crea un BLOB e visualizza il JSON da scaricare )
        * Upload ( Legge file.json da file e carica su database)
        * List ( visualizza i Clienti presentei sul database)
        * DummyMenu ( elemento html usato come una topbar, costituito da un titolo e un button che punta a Home)

# Installazione
* git clone 'questo_repository'
* $> cd linkme-test/src/

## Compilazione Typescript WebApp
* $linkme-test> cd src/linkme-app/
* $linkme-test> npm install
* $linkme-app> ng build --delete-output-path=false --output-path=../main/resources/static
* $linkme-app> cd .. && cd ..

## Compilazione Server spring
* $linkme-test> gradle build

## Avvio Server
* $linkme-test> gradle bootRun 
* http://localhost:8080 ( o altra porta impostata)
* file prova.json per test in repository

#Cosa si poteva fare meglio
* Autenticazione usando un SecurityAdapter di Spring 
* Creazione pagina errore in WebApp per redirect errori/malfunzionamenti
* Gestione layout grafico, per esempio usando una topbar 
* ...

--CIAO--